package com.kenfogel.binarytree;

import com.kenfogel.binarytree.implementation.BinaryTree;
import com.kenfogel.binarytree.presentation.BinaryTreePrinter;
import com.kenfogel.binarytree.presentation.BinaryTreePrinterConsole;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Random;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Jean-Claude Desrosiers
 */
public final class BinaryTreeAppFX extends Application {
    private BinaryTree<Integer> tree;

    public static void main(String[] args) {
        (new BinaryTreeAppFX()).startWithMode("cli", args);
    }

    private void startWithMode(String mode, String[] args) {
        initTree();

        if (mode.equalsIgnoreCase("gui")) {
            launch(args);
        }
        else if (mode.equalsIgnoreCase("cli")) {
            start(System.out, args);
        }
        else {
            throw new UnsupportedOperationException(
                    "Mode : [" + mode + "] not supported");
        }
    }

    private void initTree() {
        tree = new BinaryTree<>(Comparator.<Integer>naturalOrder());

        Random rand = new Random();
        int iter = rand.nextInt(15) + 15;
        for (int i = 0; i < iter; i++) {
            tree.insert(rand.nextInt(200) - 100);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }

    private void start(PrintStream outputStream, String[] args) {
        BinaryTreePrinter console = new BinaryTreePrinterConsole(outputStream);

        console.print(tree);
    }
}
