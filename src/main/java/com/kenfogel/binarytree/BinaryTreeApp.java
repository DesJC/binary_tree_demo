package com.kenfogel.binarytree;

import com.kenfogel.binarytree.implementation.BinaryTree;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 */
public class BinaryTreeApp {

    private BinaryTree<Integer> tree;

    /**
     * Run the various methods in a tree to test it.
     */
    public void perform() {
        tree = new BinaryTree<>(Integer::compare);

        Integer[] data = {
            1, 56, 34, 78, 2, 6, -45, 45, 0, 123, 65, 43, 87, 90
        };

        buildATree(data);

        System.out.println("Look up \"salesperson\" = "
                + tree.lookup(1));
        System.out.println("Look up \"fire\" = "
                + tree.lookup(99));
        System.out.println("Size of tree = " + tree.size());
        System.out.println("Max depth = " + tree.maxDepth());
        System.out.println("Min value = " + tree.minValue());

        System.out.print("Inorder = ");
        tree.printInorderTree();
        System.out.print("Postorder = ");
        tree.printPostorder();
        System.out.println("The Paths");
        tree.printPaths();
        System.out.println("The Tree");
        tree.printLineByLine();
    }

    /**
     * Build a tree by inserting the members of the array into the tree.
     *
     * @param data
     */
    public void buildATree(Integer data[]) {
        for (Integer singleDataPoint : data) {
            tree.insert(singleDataPoint);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        BinaryTreeApp bta = new BinaryTreeApp();
        bta.perform();
        System.exit(0);
    }
}
