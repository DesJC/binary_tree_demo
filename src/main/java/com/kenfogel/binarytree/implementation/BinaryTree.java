package com.kenfogel.binarytree.implementation;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.Queue;
import java.util.stream.IntStream;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 * @param <T> Type of data contained by each node
 */
public class BinaryTree<T> {

    // Root node reference. Will be null for an empty tree.
    private BinaryTreeNode<T> root;

    private final Comparator<T> COMPARATOR;

    /**
     * Creates an empty binary tree -- a null root reference.
     *
     * @param comparator defines the order of the nodes
     */
    public BinaryTree(Comparator<T> comparator) {
        root = null;
        COMPARATOR = comparator;
    }

    /**
     * Inserts the given data into the binary tree.Uses a recursive helper.
     *
     * @param data
     */
    public void insert(T data) {
        root = insert(root, data);
    }

    private BinaryTreeNode insert(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            node = new BinaryTreeNode(data);
        }

        else {
            if (COMPARATOR.compare(data, node.data) <= 0) {
                node.left = insert(node.left, data);
            }
            else {
                node.right = insert(node.right, data);
            }
        }

        return (node); // in any case, return the new reference to the caller
    }

    /**
     * Returns true if the given target is in the binary tree.Uses a recursive
     * helper.
     *
     * @param data
     *
     * @return true of false depending on whether the data is found
     */
    public boolean lookup(T data) {
        return (lookup(root, data));
    }

    /**
     * Recursive lookup -- given a node, recur down searching for the given
     * data.
     */
    private boolean lookup(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            return (false);
        }

        if (COMPARATOR.compare(data, node.data) == 0) {
            return (true);
        }
        else if (COMPARATOR.compare(data, node.data) < 0) {
            return (lookup(node.left, data));
        }
        else {
            return (lookup(node.right, data));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * Returns the number of nodes in the tree. Uses a recursive helper that
     * recurses down the tree and counts the nodes.
     *
     * @return the number of elements in the tree
     */
    public int size() {
        return (size(root));
    }

    private int size(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        }
        else {
            return (size(node.left) + 1 + size(node.right));
        }
    }

    /**
     * Returns the max root-to-leaf depth of the tree. Uses a recursive helper
     * that recurses down to find the max depth.
     *
     * @return The depth of the tree from the root to the lowest node
     */
    public int maxDepth() {
        return (maxDepth(root));
    }

    private int maxDepth(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        }
        else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            // use the larger + 1 
            return (Math.max(lDepth, rDepth) + 1);
        }
    }

    /**
     * Returns the min value in a non-empty binary search tree. Uses a helper
     * method that iterates to the left to find the min value.
     *
     * @return The smallest value in the tree
     */
    public T minValue() {
        return (minValue(root));
    }

    /**
     * Finds the min value in a non-empty binary search tree.
     */
    private T minValue(BinaryTreeNode<T> node) {
        BinaryTreeNode<T> current = node;
        while (current.left != null) {
            current = current.left;
        }

        return (current.data);
    }

    /**
     * Prints the node values in the "inorder" order. Uses a recursive helper to
     * do the traversal.
     */
    public void printInorderTree() {
        printInorderTree(root);
        System.out.println();
    }

    private void printInorderTree(BinaryTreeNode node) {
        if (node == null) {
            return;
        }

        // left, node itself, right 
        printInorderTree(node.left);
        System.out.print(node.data + "  ");
        printInorderTree(node.right);
    }

    /**
     * Prints the node values in the "postorder" order. Uses a recursive helper
     * to do the traversal.
     */
    public void printPostorder() {
        printPostorder(root);
        System.out.println();
    }

    private void printPostorder(BinaryTreeNode node) {
        if (node == null) {
            return;
        }

        // first recur on both subtrees 
        printPostorder(node.left);
        printPostorder(node.right);

        // then deal with the node 
        System.out.print(node.data + "  ");
    }

    /**
     * Given a binary tree, prints out all of its root-to-leaf paths, one per
     * line. Uses a recursive helper to do the work.
     */
    public void printPaths() {
        T[] path = (T[]) new Object[maxDepth()];
        printPaths(root, path, 0);
    }

    /**
     * Recursive printPaths helper -- given a node, and an array containing the
     * path from the root node up to but not including this node, prints out all
     * the root-leaf paths.
     */
    private void printPaths(BinaryTreeNode<T> node, T[] path, int pathLen) {
        if (node == null) {
            return;
        }

        // append this node to the path array 
        path[pathLen] = node.data;
        pathLen++;

        // it's a leaf, so print the path that led to here 
        if (node.left == null && node.right == null) {
            printArray(path, pathLen);
        }
        else {
            // otherwise try both subtrees 
            printPaths(node.left, path, pathLen);
            printPaths(node.right, path, pathLen);
        }
    }

    /**
     * Utility that prints ints from an array on one line.
     */
    private void printArray(T[] dataPoints, int len) {
        int i;
        for (i = 0; i < len; i++) {
            System.out.print(dataPoints[i].toString() + " ");
        }
        System.out.println();
    }

    /**
     * Prints out each level in the tree from left to right. Uses a recursive
     * helper to do the work.
     */
    public void printLineByLine() {
        T[][] lines = getLineByLine();

        for (T[] level : lines) {
            for (T dataPoint : level) {
                if (dataPoint != null) {
                    System.out.print(dataPoint.toString() + " ");
                }
                else {
                    System.out.print("\"\" ");
                }
            }

            System.out.println();
        }
    }

    public T[][] getLineByLine() {
        T[][] result = (T[][]) new Object[maxDepth()][];
        if (root != null) {
            final Queue<Optional<BinaryTreeNode<T>>> tempQueue =
                    new ArrayDeque<>();
            tempQueue.add(Optional.of(root));

            IntStream.range(0, result.length)
                    .forEachOrdered(level -> {
                        final int size = (int) Math.pow(2, level);
                        result[level] = (T[]) new Object[size];

                        IntStream.range(0, size)
                                .forEachOrdered(index -> {
                                    Optional<BinaryTreeNode<T>> nextInQueue =
                                            tempQueue.poll();

                                    if (nextInQueue.isPresent()) {
                                        result[level][index] =
                                                nextInQueue.get().data;

                                        tempQueue.add(Optional.ofNullable(
                                                nextInQueue.get().left));
                                        tempQueue.add(Optional.ofNullable(
                                                nextInQueue.get().right));
                                    }
                                    else {
                                        result[level][index] = null;

                                        tempQueue.add(Optional.empty());
                                        tempQueue.add(Optional.empty());
                                    }
                                });
                    });
        }

        return result;
    }
}
