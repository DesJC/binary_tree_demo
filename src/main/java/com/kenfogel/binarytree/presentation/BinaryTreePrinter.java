package com.kenfogel.binarytree.presentation;

import com.kenfogel.binarytree.implementation.BinaryTree;

/**
 *
 * @author Jean-Claude Desrosiers
 */
public interface BinaryTreePrinter {
    <T> void print(BinaryTree<T> tree);
}
