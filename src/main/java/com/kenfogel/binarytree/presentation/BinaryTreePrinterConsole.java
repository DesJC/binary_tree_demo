package com.kenfogel.binarytree.presentation;

import com.kenfogel.binarytree.implementation.BinaryTree;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.stream.IntStream;

/**
 *
 * @author Jean-Claude Desrosiers
 */
public final class BinaryTreePrinterConsole implements BinaryTreePrinter {
    private final PrintStream OUT;
    private final static int NODE_STRING_LENGTH = 5;

    public BinaryTreePrinterConsole(PrintStream out) {
        OUT = out;
    }

    @Override
    public <T> void print(BinaryTree<T> tree) {
        final T[][] lines = tree.getLineByLine();
        final Deque<String> reversedTree = new ArrayDeque<>();

        int prevSpacing;
        int spacing = 1;
        int beginPad = 0;
        for (int i = lines.length - 1; i >= 0; i--) {
            String lineStr = pad("", beginPad, 0);

            // data nodes row
            for (T dataPoint : lines[i]) {
                lineStr += pad(dataPointToString(dataPoint), 0, spacing);
            }
            reversedTree.add(lineStr);

            prevSpacing = spacing;
            spacing = (prevSpacing * 2) + NODE_STRING_LENGTH;

            // branches (/ OR \) row
            if (i > 0) {
                int branchHeight = (prevSpacing + NODE_STRING_LENGTH) / 2 - 2;
                for (int j = 0; j < branchHeight; j++) {
                    String branchRow = pad("", 0,
                            beginPad + NODE_STRING_LENGTH + j - 1);
                    int branchSpacingAfterLeft = prevSpacing - (2 * j);
                    int branchSpacingAfterRight =
                            (spacing - (branchHeight - j) * 2) + 4;

                    boolean isLeftBranch = true;
                    // branch character for every node
                    for (T dataPoint : lines[i]) {
                        branchRow += dataPoint == null ? " "
                                : (isLeftBranch ? "/" : "\\");

                        if (isLeftBranch) {
                            branchRow += pad("", 0, branchSpacingAfterLeft);
                        }
                        else {
                            branchRow += pad("", 0, branchSpacingAfterRight);
                        }

                        isLeftBranch = !isLeftBranch;
                    }

                    reversedTree.add(branchRow);
                }
            }

            beginPad = prevSpacing + (NODE_STRING_LENGTH / 2);
        }

        while (!reversedTree.isEmpty()) {
            OUT.println(reversedTree.removeLast());
        }
    }

    private <T> String dataPointToString(T dataPoint) {
        if (dataPoint == null) {
            return pad("", 0, NODE_STRING_LENGTH);
        }

        String dataStr = dataPoint.toString();

        if (dataStr.length() > NODE_STRING_LENGTH) {
            dataStr = dataStr.substring(0, NODE_STRING_LENGTH - 1);

            dataStr += "\u2026";
        }
        else {
            int beginPad =
                    (NODE_STRING_LENGTH - dataStr.length()) / 2;
            int endPad = NODE_STRING_LENGTH - dataStr.length() - beginPad;

            dataStr = pad(dataStr, beginPad, endPad);
        }

        return dataStr;
    }

    private String pad(String toPad, int beginPad, int endPad) {
        for (; beginPad > 0 && endPad > 0; beginPad--, endPad--) {
            toPad = " " + toPad + " ";
        }

        for (; beginPad > 0; beginPad--) {
            toPad = " " + toPad;
        }

        for (; endPad > 0; endPad--) {
            toPad += " ";
        }

        return toPad;
    }
}
